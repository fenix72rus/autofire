﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class F3DCharacter : MonoBehaviour
{
    public int Health;
    public UnityAction OnDamageEvent, OnDeadEvent;

    [SerializeField] private bool isPlayer;

    private Animator _animator;
    private F3DCharacterController _controller;
    private int _hitTriggerCounter;
    private float _hitTriggerTimer;
    private Rigidbody2D _rBody;
    private F3DWeaponController _weaponController;
    private bool _isDead;

    // Use this for initialization
    void Awake()
    {
        _rBody = GetComponent<Rigidbody2D>();
        _controller = GetComponent<F3DCharacterController>();
        _weaponController = GetComponent<F3DWeaponController>();
        _animator = GetComponent<Animator>();
    }

    public void OnDead()
    {
        OnDeadEvent?.Invoke();

        if(isPlayer)
            Observer.Instance.OnPlayerDeath?.Invoke();
        Health = 0;
        _isDead = true;

        if(_animator)
            _animator.SetBool(Random.Range(-1f, 1f) > 0 ? "DeathFront" : "DeathBack", true);

        if (_controller)
        {
            // Player Death sequence

            // Dead dont do shit
            _controller.enabled = false;

            // Disable blob shadow under the character
            if (_controller.Shadow)
                _controller.Shadow.enabled = false;
        }

        gameObject.layer = LayerMask.NameToLayer("Dead");
        _rBody.drag = 2f;

        //            for (int i = 0; i < _colliders.Length; i++)
        //                _colliders[i].enabled = false;
        if (_weaponController)
            _weaponController.Drop();
    }

    public void OnDamage(int damageAmount)
    {
        //if (_controller == null) return;
        if (_isDead) 
            return;

        OnDamageEvent?.Invoke();

        // Substract incoming damage
        if (Health > 0)
            Health -= damageAmount;

        // Dead Already?
        if (Health <= 0)
        {
            OnDead();
            //
            return;
        }

        // Play Hit Animation and limit hit animation triggering 
        if(_controller)
        {
            if (_hitTriggerCounter < 1)
                if(_controller.Character)
                    _controller.Character.SetTrigger("Hit");
        }
        _hitTriggerCounter++;

    }

    private void LateUpdate()
    {
        // Dead... Quit trying
        if (_isDead) return;
    //    if (Input.GetKeyDown(KeyCode.K))
    //        OnDamage(1000);

        // Handle Hit Trigger timer
        if (_hitTriggerTimer > 0.5f) // <- Hit timer
        {
            _hitTriggerCounter = 0;
            _hitTriggerTimer = 0;
        }
        _hitTriggerTimer += Time.deltaTime;
    }
}