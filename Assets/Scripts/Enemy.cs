using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(F3DCharacter))]
[RequireComponent(typeof(F3DDamage))]
[RequireComponent(typeof(HealthBar))]
public class Enemy : MonoBehaviour
{
    public float MaxHealth
    {
        get { return _maxHealth; }

        set
        {
            if (value <= 0)
                Debug.LogError("Health <= 0");

            _maxHealth = value;
        }
    }

    public Transform Transform 
    { 
        get
        {
            if (proxy)
                return proxy;
            else
                return _transform;
        }
    }

    [SerializeField] private Transform proxy;

    private HealthBar _healthBar;
    private Transform _transform;
    private F3DCharacter _character;
    private float _maxHealth;

    private void Awake()
    {
        Init();
    }

    protected void Init()
    {
        _transform = this.transform;

        _healthBar = GetComponent<HealthBar>();

        _character = GetComponent<F3DCharacter>();
        _character.OnDamageEvent += TakeDamage;
        _character.OnDeadEvent += Dead;
        _character.OnDeadEvent += _healthBar.DestroyBar;
        MaxHealth = _character.Health;
    }

    public void TakeDamage()
    {
        _healthBar.UpdateHealth(_character.Health, MaxHealth);
    }

    public void Dead()
    {
        LevelManager.Instance.EnemyDestroyed(this);
        Collider2D[] colliders = this.GetComponents<Collider2D>();
        foreach (var collider in colliders)
            collider.enabled = false;
        Destroy(this.gameObject, 2f);
    }
}
