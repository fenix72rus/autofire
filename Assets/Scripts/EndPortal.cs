using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class EndPortal : MonoBehaviour
{
    public UnityEvent OnEnteredPortal;

    [SerializeField] private bool lastLevel;
    [SerializeField] private GameObject shield;

    private void Start()
    {
        Observer.Instance.OnLevelWon.AddListener(EnablePortal);
    }

    public void EnablePortal()
    {
        Destroy(shield);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.TryGetComponent(out Player player))
        {
            OnEnteredPortal?.Invoke();
        }
    }
}
