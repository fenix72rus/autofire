using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(F3DCharacter))]
[RequireComponent(typeof(F3DDamage))]
[RequireComponent(typeof(HealthBar))]
public class Player : MonoBehaviour
{
    public float MaxHealth
    {
        get { return _maxHealth; }

        set
        {
            if (value <= 0)
                Debug.LogError("Health <= 0");

            _maxHealth = value;
        }
    }
    public F3DCharacter Character { get => _character; }

    public Transform Transform
    {
        get
        {
            if (_transform)
                return _transform;
            else
                return this.transform;
        }
    }

    public Transform Proxy;

    [SerializeField] private TextMessageShower messageShower;

    private Transform _transform;
    private HealthBar _healthBar;
    private F3DCharacter _character;
    private float _maxHealth;

    private void Awake()
    {
        Init();
    }

    protected void Init()
    {
        if (!_transform)
            _transform = this.transform;

        _healthBar = GetComponent<HealthBar>();

        _character = GetComponent<F3DCharacter>();
        _character.OnDamageEvent += TakeDamage;
        _character.OnDeadEvent += Dead;
        _character.OnDeadEvent += _healthBar.DestroyBar;
        MaxHealth = _character.Health;
    }
    public void TakeDamage()
    {
        _healthBar.UpdateHealth(_character.Health, MaxHealth);
    }

    public void SetMaxHealth(float maxHealthValue)
    {
        MaxHealth = maxHealthValue;
        _healthBar.UpdateHealth(_character.Health, MaxHealth);
    }

    public void Dead()
    {
        Debug.Log("Player dead");
    }

    private void Start()
    {
        LevelManager.Instance.OnPlayerInstantiateEvent?.Invoke(this);
    }

    public void ShowMessage(string message)
    {
        messageShower.ShowMessage(message);
    }
}
