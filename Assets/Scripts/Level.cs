using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public List<Enemy> Enemies { get => enemies; }
    public Vector3 PlayerStartPosition { get => playerStartPosition; }

    [SerializeField] private bool hasAward;
    [SerializeField] private List<Enemy> enemies;
    [SerializeField] private Vector3 playerStartPosition = new Vector3(-60, -6, 0);

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        LevelManager.Instance.InitializeLevel(this);
        if (hasAward)
            Observer.Instance.UIManager.SubscribeToAward();
    }
 
    public void DestoyLevel()
    {
        Destroy(this.gameObject);
    }

    public void OnWorldEnded()
    {
        Observer.Instance.OnGameEnded?.Invoke();
    }
}
