using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardChest : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.GetComponent<Player>())
        {
            Observer.Instance.ShowAdsPanel();
            Destroy(this.gameObject);
        }
    }
}
