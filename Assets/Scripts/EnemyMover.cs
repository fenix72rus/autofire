using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{
    [SerializeField] private Transform[] pathPoints;
    [SerializeField] private float stopDistance = 2f;

    private int _currentPointNumber;
    private Transform _transform;
    private bool _reversePath;

    private void Awake()
    {
        _transform = this.transform;
    }

    public Vector2 GetMoveDirection()
    {
        var targetPoint = pathPoints[_currentPointNumber];

        if (Vector2.Distance(_transform.position, targetPoint.position) < stopDistance)
        {
            NextPathPoint();
        }

        var direction = (targetPoint.position - _transform.position).normalized;
        return direction;
    }

    private void NextPathPoint()
    {
        if(_reversePath)
        {
            _currentPointNumber--;
            if (_currentPointNumber < 0)
            {
                _reversePath = false;
                _currentPointNumber = 1;
            }
        }
        else
        {
            _currentPointNumber++;
            if (_currentPointNumber >= pathPoints.Length)
            {
                _reversePath = true;
                _currentPointNumber = pathPoints.Length - 2;
            }
        }
    }
}
