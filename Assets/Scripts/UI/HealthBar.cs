using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image filledImage;
    [SerializeField] private Canvas worldCanvas;

    public void UpdateHealth(float currentHealth, float maxHealth)
    {
        currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);

        if (currentHealth <= 0)
        {
            DestroyBar();
        }

        float absoluteValue = currentHealth / maxHealth;

        filledImage.fillAmount = absoluteValue;
    }

    public void DestroyBar()
    {
        Destroy(worldCanvas.gameObject);
    }
}
