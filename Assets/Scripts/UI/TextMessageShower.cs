using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;


public class TextMessageShower : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI messageText;
    [SerializeField] private RectTransform messageRect;

    private void Awake()
    {
        if (!messageRect)
            messageRect = this.GetComponent<RectTransform>();
        if (!messageText)
            messageText = this.GetComponent<TextMeshProUGUI>();
    }

    public void ShowMessage(string message)
    {
        messageText.text = message;
        StartCoroutine(MessageAnimation());
    }

    private IEnumerator MessageAnimation()
    {
        var startScale = messageRect.localScale;
        yield return new WaitForSeconds(1.5f);
        messageRect.DOScale(Vector2.zero, 0.75f);
        yield return new WaitForSeconds(0.75f);
        messageText.text = "";
        messageRect.localScale = startScale;
    }
}
