using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public GameObject AdsPanel { get => adPanel; }
    public JumpButton JumpButton { get => jumpButton; }

    [SerializeField] private TextMeshProUGUI coinsText, gemsText;
    [SerializeField] private JumpButton jumpButton;
    [SerializeField] private Button addMaxHealthButton, addDamageButton, addFireRateButton;
    [SerializeField] private FixedJoystick fixedJoystick;
    [SerializeField] private GameObject awardPanel, adPanel;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        LevelManager.Instance.OnPlayerInstantiateEvent += SetInputs;
        coinsText.text = Bank.Coins.ToString();
        gemsText.text = Bank.Gems.ToString();
    }

    public void SubscribeToAward()
    {
        Observer.Instance.OnLevelWon.AddListener(ShowAwardPanel);
    }

    public void ShowAwardPanel()
    {
        awardPanel.SetActive(true);
        Observer.Instance.OnLevelWon.RemoveListener(ShowAwardPanel);
    }

    private void SetInputs(Player player)
    {
        var controller = player.GetComponent<F3DCharacterController>();
        jumpButton.OnJumpPressed.AddListener(controller.Jump);
        jumpButton.gameObject.SetActive(true);
        controller.Joystick = fixedJoystick;
        fixedJoystick.gameObject.SetActive(true);

        var improvement = player.GetComponent<CharacterImprovements>();
        addMaxHealthButton.onClick.AddListener(improvement.AddMaxHealth);
        addDamageButton.onClick.AddListener(improvement.AddDamage);
        addFireRateButton.onClick.AddListener(improvement.AddRateFire);
    }
}
