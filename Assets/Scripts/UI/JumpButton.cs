using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class JumpButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public UnityEvent OnJumpPressed;
    public bool OnJumpHolded;

    public void OnPointerDown(PointerEventData eventData)
    {
        OnJumpHolded = true;
        OnJumpPressed?.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnJumpHolded = false;
    }
}
