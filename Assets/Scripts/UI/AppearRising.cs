using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AppearRising : MonoBehaviour
{
    private RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = this.GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        var startScale = rectTransform.localScale;
        rectTransform.localScale = Vector3.zero;

        Sequence sequence = DOTween.Sequence();
        sequence.Append(rectTransform.DOScale(startScale * 1.1f, 1.25f));
        sequence.Append(rectTransform.DOScale(startScale, 0.5f));
    }
}
