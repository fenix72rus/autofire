using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticDontDestroy : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}
