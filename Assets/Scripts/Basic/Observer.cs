using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Observer : MonoBehaviour
{
    public static Observer Instance;

    public OnValueAdded OnCoinsAdded, OnGemsAdded;
    public UnityEvent OnLevelWon, OnLevelInit, OnGameStarted, OnGameEnded, OnPlayerDeath;
    public UIManager UIManager;

    public delegate void OnValueAdded(int value);

    [SerializeField] private AdsShower adsShower;

    private void Awake()
    {
        Instantiate();
    }

    private void Instantiate()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.transform.parent.gameObject);
        }

        OnCoinsAdded += Bank.AddCoins;
        OnGemsAdded += Bank.AddGems;
        //OnLevelWon.AddListener(adsShower.RequestAndLoadRewardedAd);
        //adsShower.OnUserEarnedRewardEvent.AddListener(UIManager.ShowAwardPanel);
    }

    public void LoadLevelScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);

        OnGameStarted?.Invoke();
    }

    public void ShowAdsPanel()
    {
        UIManager.AdsPanel.SetActive(true);
    }

    public void ShowRewardAd()
    {
        //adsShower.ShowRewardedAd();
    }
}
