using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    public delegate void OnPlayerInstantiate(Player player);

    public Player Player { get; private set; }
    public List<Enemy> Enemies 
    { 
        get
        {
            if (currentLevel)
                return currentLevel.Enemies;
            else
                return null;
        }
    }

    public OnPlayerInstantiate OnPlayerInstantiateEvent;

    private Level currentLevel;

    [SerializeField] private int maxCoinsAward = 20;
    [SerializeField] private int minCoinsAward = 5;

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        OnPlayerInstantiateEvent += SetPlayer;
    }

    private void Start()
    {
        SubcscribeToObserver();
    }

    private void SubcscribeToObserver()
    {
        Observer.Instance.OnLevelWon.AddListener(WinLevel);
    }

    public void SetPlayer(Player player)
    {
        this.Player = player;
    }

    public void InitializeLevel(Level level)
    {
        if (currentLevel)
            Destroy(currentLevel.gameObject);

        currentLevel = level;

        if(!Player)
        {
            var playerPrefab = Resources.Load<GameObject>("Player");
            OnPlayerInstantiateEvent?.Invoke(Instantiate(playerPrefab).GetComponent<Player>());
        }
        Player.Transform.position = currentLevel.PlayerStartPosition;

        Observer.Instance.OnLevelInit?.Invoke();
    }

    public void EnemyDestroyed(Enemy enemy)
    {
        try
        {
            currentLevel.Enemies.Remove(enemy);
        }
        catch
        {
            Debug.LogError($@"����� {enemy} ��� � �������");
            return;
        }

        currentLevel.Enemies.RemoveAll(x => x == null);

        if (currentLevel.Enemies.Count == 0)
            Observer.Instance.OnLevelWon?.Invoke();

    }

    public void WinLevel()
    {
        Debug.Log("Level won");
        AddRandomCoinsValue();
    }

    public void LoadMainMenuScene()
    {
        SceneManager.LoadScene(0);
    }

    public void AddRandomCoinsValue()
    {
        var coinsCount = Random.Range(minCoinsAward, maxCoinsAward + 1);
        Bank.Coins += coinsCount;
        Player.ShowMessage($"+{coinsCount} Coins");
    }
}
