using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bank
{
    private const string COINS_HAS_KEY = "Coins";
    private const string GEMS_HAS_KEY = "Gems";


    public static int Coins
    {
        get => GetIntegerValue(COINS_HAS_KEY);
        set => SetIntegerValue(COINS_HAS_KEY, value);
    }

    public static int Gems
    {
        get => GetIntegerValue(GEMS_HAS_KEY);
        set => SetIntegerValue(GEMS_HAS_KEY, value);
    }


    private static int GetIntegerValue(string key)
    {
        if(PlayerPrefs.HasKey(key))
        {
            return PlayerPrefs.GetInt(key);
        }
        else
        {
            return 0;
        }
    }

    private static void SetIntegerValue(string key, int value)
    {
        if(value < 0)
            value = 0;
        PlayerPrefs.SetInt(key, value);
    }

    public static void AddCoins(int value)
    {
        Coins += value;
    }

    public static void AddGems(int value)
    {
        Gems += value;
    }

    /*private static void AddIntegerValue(string key, int value)
    {
        if (PlayerPrefs.HasKey(key))
        {
            var previousValue = PlayerPrefs.GetInt(key);
            if (previousValue + value < 0)
                value = 0;

            PlayerPrefs.SetInt(key, previousValue + value);
        }
        else
        {
            if (value < 0)
                value = 0;

            PlayerPrefs.SetInt(key, value);
        }
    }*/
}
