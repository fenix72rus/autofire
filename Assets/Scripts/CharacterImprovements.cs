using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterImprovements : MonoBehaviour
{
    [SerializeField] private F3DGenericWeapon assault;
    [SerializeField] private Player player;

    public void AddRateFire(float modifire)
    {
        if (modifire <= 0)
            modifire = 1f;

        assault.FireRateValue = assault.FireRateValue * modifire;
    }

    public void AddRateFire()
    {
        assault.FireRateValue = assault.FireRateValue * 1.25f;
    }

    public void AddDamage(float modifire)
    {
        if (modifire <= 0)
            modifire = 1f;

        assault.Damage = (int)(assault.Damage * modifire);
    }

    public void AddDamage()
    {
        assault.Damage = (int)(assault.Damage * 1.25f);
    }

    public void AddMaxHealth(float modifire)
    {
        if (modifire <= 0)
            modifire = 1f;

        player.SetMaxHealth(player.MaxHealth * modifire);
    }

    public void AddMaxHealth()
    {
        player.SetMaxHealth(player.MaxHealth * 1.2f);
    }
}
