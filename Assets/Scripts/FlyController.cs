using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyController : MonoBehaviour
{
    public bool ReadyToShoot { get => _readyToShoot; }

    public float MaxVelocityX;
    public float MaxVelocityY;
    public float MoveForce = 365f;
    public float MaxSpeed = 1f;
    public float AimTime;
    public float CrouchSpeed = 0.75f;
    public float MaxSpeedBackwards = 0.5f;
    public float MaxSpeedFade = 1f;

    [SerializeField] private Transform WeaponSocket;
    [SerializeField] private bool shootingWhileMove;
    [SerializeField] private EnemyMover enemyMover;
    [SerializeField] private LayerMask shotIgnoreLayers;

    private bool _readyToShoot;
    private bool _crouch;
    private bool _facingRight = true;
    private float _horizontalSignLast;
    private float _hClamp;
    private float _speed;
    private F3DWeaponController _weaponController;
    private Rigidbody2D _rb2D;
    private F3DCharacter _character;
    private Transform _transform;
    private Vector3 _direction;
    [SerializeField] private bool _shootDelay;
    private IEnumerator shootRutine;

    private void Awake()
    {
        _transform = this.transform;
        _character = GetComponent<F3DCharacter>();
        _weaponController = GetComponent<F3DWeaponController>();
        _rb2D = GetComponent<Rigidbody2D>();
        _speed = MaxSpeed;
    }

    private void Update()
    {
        if (shootRutine == null)
        {
            shootRutine = ShootRutine();
            StartCoroutine(shootRutine);
        }

        if(_shootDelay)
        {
            Move(enemyMover.GetMoveDirection());
        }
        else
        {
            Shoot();
            _hClamp = 0;
        }
    }

    public void Move(Vector3 direction)
    {
        _direction = direction;

        if (_direction.x > 0)
        {
            if (!_facingRight && !ReadyToShoot)
                Flip();

            _hClamp = 1f;
        }
        else if (_direction.x < 0)
        {
            if (_facingRight && !ReadyToShoot)
                Flip();

            _hClamp = 1f;
        }
        else
        {
            _hClamp = 0;
        }
        _weaponController.SetFloat("Horizontal", Mathf.Abs(_hClamp));

        WeaponSocket.rotation = Quaternion.Slerp(Quaternion.identity, WeaponSocket.rotation, 0.1f * Time.deltaTime);
    }


    private void FixedUpdate()
    {
        var velocityClamp = _rb2D.velocity;
        _rb2D.AddForce(_direction * _hClamp * MoveForce, ForceMode2D.Force);
        velocityClamp.x = Mathf.Clamp(_rb2D.velocity.x, -MaxVelocityX * _speed,
            MaxVelocityX * _speed);
        velocityClamp.y = Mathf.Clamp(_rb2D.velocity.y, -MaxVelocityY, MaxVelocityY);

        _rb2D.velocity = velocityClamp;


    }

    private IEnumerator ShootRutine()
    {
        _shootDelay = false;
        _rb2D.velocity = Vector3.zero;
        yield return new WaitForSeconds(3f);
        _weaponController.StopFire();
        _shootDelay = true;
        yield return new WaitForSeconds(5f);
        shootRutine = null;
    }

    private void Shoot()
    {
        var fireDirection = GetTargetDirection();
        Firing(fireDirection);

        if (ReadyToShoot && !_weaponController.IsShooting)
            _weaponController.StartFire();
        if (!ReadyToShoot && _weaponController.IsShooting)
            _weaponController.StopFire();
    }

    public Vector3 GetTargetDirection()
    {
        var player = LevelManager.Instance.Player;
        _readyToShoot = false;

        if (player)
        {
            var hittedCollider = ShotRaycast(player.Proxy.position);
            if (hittedCollider)
            {
                if (hittedCollider.transform.TryGetComponent(out Player hittedPlayer))
                {
                    if (hittedPlayer == player)
                    {
                        _readyToShoot = true;
                    }
                }
            }
        }

        if (_readyToShoot)
        {
            return player.Proxy.position;
        }
        else
        {
            return Vector3.zero;
        }
    }

    private RaycastHit2D ShotRaycast(Vector3 originPosition)
    {
        var direction = (originPosition - WeaponSocket.position).normalized;
        var hittedCollider = Physics2D.Raycast(WeaponSocket.position, direction, 60f, shotIgnoreLayers);
        return hittedCollider;
    }

    public void Firing(Vector3 targetPosition)
    {
        if (WeaponSocket == null) return;

        var aimPos = targetPosition;
        aimPos.z = 0;

        // Look direction
        var dir = (aimPos - WeaponSocket.position).normalized;
        dir.z = 0;

        // Weapon socket to FX Socket offset
        var currentWeapon = _weaponController.GetCurrentWeapon();
        if (currentWeapon.Type == F3DWeaponController.WeaponType.Melee)
        {
            WeaponSocket.rotation = Quaternion.identity;
        }
        else
        {
            var offset = currentWeapon.FXSocket.position - WeaponSocket.position;
            offset.z = 0;
            var localOffset = WeaponSocket.InverseTransformVector(offset);
            localOffset.x = 0;
            localOffset.z = 0;
            Debug.DrawLine(Vector3.zero, localOffset * transform.lossyScale.x, Color.yellow);

            //  Debug.DrawLine(WeaponSocket.position, currentWeapon.FXSocket.position, Color.yellow);
            var worldOffset = WeaponSocket.TransformVector(localOffset) - WeaponSocket.right * 5 * Mathf.Sign(dir.x);
            var weaponDir = (aimPos - (WeaponSocket.position + worldOffset)).normalized;
            var socketRotation = Quaternion.LookRotation(Vector3.forward,
                Mathf.Sign(dir.x) * Vector3.Cross(Vector3.forward, weaponDir));
            WeaponSocket.rotation = Quaternion.Lerp(WeaponSocket.rotation, socketRotation, Time.deltaTime * AimTime);

            // Lock Weapon Socket Angle
            var rot = WeaponSocket.rotation;
            const float z = 0.35f;
            if (_facingRight && WeaponSocket.rotation.z < -z)
            {
                rot.z = -z;
                WeaponSocket.rotation = rot;
            }
            else if (!_facingRight && WeaponSocket.rotation.z > z)
            {
                rot.z = z;
                WeaponSocket.rotation = rot;
            }
        }

        // Flip
        if (dir.x > 0 && !_facingRight)
            Flip();
        else if (dir.x < 0 && _facingRight)
            Flip();

        // Draw Velocity
        Debug.DrawLine(currentWeapon.FXSocket.position, aimPos, Color.blue);

        // Check Facing
        if (_hClamp > 0) _horizontalSignLast = 1f;
        else if (_hClamp < 0) _horizontalSignLast = -1f;
        var facingSing = (_facingRight ? 1f : -1f) * _horizontalSignLast;

        // Dampen Speed on Moving Backwards and Crouch
        var speedDamp = _crouch ? CrouchSpeed : MaxSpeed;
        speedDamp = facingSing >= 0 ? speedDamp : speedDamp * MaxSpeedBackwards;
        _speed = Mathf.Lerp(_speed, speedDamp, Time.deltaTime * MaxSpeedFade);

        _weaponController.SetFloat("facingRight", facingSing);

        //
        var platformVelocity = _rb2D.velocity;
        _weaponController.SetFloat("Speed", Mathf.Abs(platformVelocity.x));
        _weaponController.SetFloat("vSpeed", platformVelocity.y);
    }

    private void Flip()
    {
        _facingRight = !_facingRight;
        var theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
